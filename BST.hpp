/*Copyright (C) 
 * 2021 - francisco dot rodriguez at ingenieria dot unam dot edu
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#pragma once

#include <cstddef>
#include "Node.hpp"

template<typename T>
class BST
{
private:
	Node<T>* root = nullptr;
	size_t len = 0L;

	Node<T>* new_node(    T& item );
	Node<T>* insert_node( Node<T>* node, T& item );
	Node<T>* seek_node(   Node<T>* node, T& item );
	void     clear_all(   Node<T>* node );
	void     inorder(     Node<T>* node, void (*visit)( T& ) );
	void     preorder(    Node<T>* node, void (*visit)( T& ) );
	void     postorder(   Node<T>* node, void (*visit)( T& ) );

   Node<T>* remove(      Node<T>* node, T& item );
   Node<T>* minValNode(  Node<T>* node );

   Node<T>* predecessor( Node<T>* node, T& key );
   Node<T>* successor(   Node<T>* node, T& key );

   Node<T>* min_val( Node<T>* node );
   Node<T>* max_val( Node<T>* node );

   size_t   calc_height( Node<T>* node, size_t height );

public:
   enum class Dir { INORDER, PREORDER, POSTORDER };
   // la vamos a usar en la función Traverse()

   BST();

   // TODO: agregar un constructor que reciba una colección de items

   ~BST();

   bool Insert( T item );
   bool Remove( T& item );
   bool Seek( T& item );
   void Traverse( BST::Dir dir, void (*visit)( T& item ) );
   void Delete_all();
   size_t Len() { return this->len; }

   T& Min();
   T& Max();

   T& Predecessor( T& key );
   T& Successor( T& key );

   Node<T>* Get_root();
   // devuelve la referencia al nodo raíz

   size_t Height();
};

