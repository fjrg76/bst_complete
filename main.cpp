/*Copyright (C) 
 * 2019 - eda1 dot fiunam @ yahoo dot com dot mx
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


#include <iostream>
// para cout, cin, y cerr

#include <fstream>
// para usar archivos


#include "Item.hpp"

template<typename T>
class BST;

#include "Node.hpp"

#include "BST.cpp"

// esta es la función que vamos a usar en cada 'visita'
void print( Item& item )
{
   std::cout << item.val << ", ";
}

void print( int& val )
{
   std::cout << val << ", ";
}


//----------------------------------------------------------------------
//  Driver program
//----------------------------------------------------------------------
int main()
{
   BST<int> arbol;

     int datos[] = { 30, 15, 45, 7, 22, 37, 52, 4, 10, 18, 26, 34, 40 };

   //for( int x : datos ){ arbol.Insert( Item( x ) ); }
   // convierte x a Item (para cuando usen tipos abstractos (clases)

   for( int x : datos ) arbol.Insert( x );
   std::cout << "Hay " << arbol.Len() << " elementos actualmente en el árbol\n";


   // NOTA: Las siguientes funciones no verifican si el árbol está vacío;
   // TAREA: escribir una función que indique si el árbol está vacío o no.
   std::cout << "El valor mínimo del árbol es: " << arbol.Min() << std::endl;
   std::cout << "El valor máximo del árbol es: " << arbol.Max() << std::endl;


   std::cout << "La altura del árbol es: " << arbol.Height() << std::endl;


   std::cout << "\n=== INORDER ===\n";
   arbol.Traverse( BST<int>::Dir::INORDER, print );
   std::cout << "\n";


   std::cout << "\n=== PREORDER ===\n";
   arbol.Traverse( BST<int>::Dir::PREORDER, [](int& val){ std::cout << val << ", "; } );
   std::cout << "\n";


   std::cout << "\n=== POSTORDER ===\n";
   arbol.Traverse( 
         BST<int>::Dir::POSTORDER, 
         
         [](int& val)
         { 
            std::cout << val << ", "; 
         } 
   );
   std::cout << "\n";


   int val;
   std::cout << "Valor a buscar\n? ";
   std::cin >> val;
   std::cout << "El valor " << val << (arbol.Seek( val ) == true ? " sí " : " no ") << "se encuentra\n";


   std::cout << "\nA punto de eliminar el item 52...\n";
   int val_to_delete( 52 );
   std::cout << (arbol.Remove( val_to_delete ) ? "Sí se eliminó\n" : "No se eliminó\n");
   std::cout << "Hay " << arbol.Len() << " elementos actualmente en el árbol\n";


   std::cout << "\n=== INORDER ===\n";
   arbol.Traverse( BST<int>::Dir::INORDER, print );
   std::cout << "\n";
}
