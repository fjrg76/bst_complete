/*Copyright (C) 
 * 2021 - francisco dot rodriguez at ingenieria dot unam dot edu
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#include <cstdio>

#include "BST.hpp"



template<typename T>
Node<T>* BST<T>::new_node( T& item )
{
   Node<T>* n = new( Node<T> );
   n->item = item;
   n->left = nullptr;
   n->right = nullptr;

   return n;
}

template<typename T>
Node<T>* BST<T>::insert_node( Node<T>* node, T& item )
{
   if( node == nullptr ) node = new_node( item ); 
   // es una hoja

   else if( item < node->item ) node->left = insert_node( node->left, item );
   // buscamos en el árbol izquierdo

   else node->right = insert_node( node->right, item );
   // buscamos en el árbol derecho

   return node;
}

template<typename T>
Node<T>* BST<T>::seek_node( Node<T>* node, T& item )
{
   if( node == nullptr or node->item == item ) return node;

   return seek_node( item < node->item ? node->left : node->right, item );
}

// utilizamos el recorrido "postorder" para borrar nodo por nodo
template<typename T>
void BST<T>::clear_all( Node<T>* node )
{
   if( node == nullptr ) return;

   clear_all( node->left );
   clear_all( node->right );
   //std::cerr << "Borrando: " << node->item << std::endl;
   delete node;
}

template<typename T>
void BST<T>::inorder( Node<T>* node, void (*visit)( T& item ) )
{
   if( node == nullptr ) return;

   inorder( node->left, visit );
   visit( node->item );
   inorder( node->right, visit );
}

template<typename T>
void BST<T>::preorder( Node<T>* node, void (*visit)( T& item ) )
{
   if( node == nullptr ) return;

   visit( node->item );
   preorder( node->left, visit );
   preorder( node->right, visit );
}

template<typename T>
void BST<T>::postorder( Node<T>* node, void (*visit)( T& item ) )
{
   if( node == nullptr ) return;

   postorder( node->left, visit );
   postorder( node->right, visit );
   visit( node->item );
}

template<typename T>
Node<T>* BST<T>::min_val( Node<T>* node )
{
   while( node->left ) node = node->left;
   return node;
}

template<typename T>
Node<T>* BST<T>::max_val( Node<T>* node )
{
   while( node->right ) node = node->right;
   return node;
}


template<typename T>
Node<T>* BST<T>::remove( Node<T>* node, T& item )
{
   if( node == nullptr ) return nullptr;

   if( item < node->item ) node->left = remove( node->left, item );

   else if( item > node->item ) node->right = remove( node->right, item );

   // si llegamos aquí es porque node.data == item

   else{

      if( node->left == nullptr )
      {
         // este caso también cubre cuando el nodo es hoja

         Node<T>* tmp = node->right;
         delete node;
         return tmp;
      } 
      else if( node->right == nullptr )
      {
         Node<T>* tmp = node->left;
         delete node;
         return tmp;
      } 
      else
      {
         Node<T>* tmp = min_val( node->right );
         node->item = tmp->item;
         node->right = remove( node->right, tmp->item );
      }
   }

   return node;
}

template<typename T>
Node<T>* BST<T>::predecessor( Node<T>* node, T& key )
{
   Node<T>* pred = nullptr;

   if( node == nullptr ) return nullptr;

   while( node != nullptr && node->key != key )
   {
      if( key < node->key )
      {
         node= node->left;
      }
      else
      {
         pred = node;
         node = node->right;
      }
   }

   if( node != nullptr && node->left != nullptr )
   {
      pred = max_val( node->left );
   }

   return predecessor;
}


template<typename T>
Node<T>* BST<T>::successor( Node<T>* node, T& key )
{
   // Tarea: ¡terminar!
}

#define MAX( x, y ) (x) > (y) ? (x) : (y)
#if 0 
size_t max_of_3( size_t a, size_t b, size_t c )
{
   return MAX( MAX( a, b ), c );
}
#endif  

template<typename T>
size_t BST<T>::calc_height( Node<T>* node, size_t height )
{
   if( node == nullptr ) return height;

   node->height = height + 1;

   size_t left_height = calc_height( node->left, node->height );
   size_t right_height = calc_height( node->right, node->height );

//   size_t ret_val = max_of_3( node->height, left_height, right_height );
   return MAX( MAX( node->height, left_height ), right_height );
}



//----------------------------------------------------------------------
//                  Interfaz implementation
//----------------------------------------------------------------------
template<typename T>
BST<T>::BST() 
{
   // nada
}

template<typename T>
BST<T>::~BST()
{
   clear_all( this->root );
   // llamamos a clear_all
}

template<typename T>
void BST<T>::Delete_all()
{
   clear_all( this->root );
   // llamamos a clear_all

   this->root = nullptr;
   this->len = 0;
   // reiniciamos los atributos de la clase
}


template<typename T>
bool BST<T>::Insert( T item )
{
   //if( Seek( item ) == true ){ return false; }
   // no se permiten duplicados!
   // ¿existirá una mejor forma de hacerlo para no recorrer dos veces el árbol?

   if( this->root != nullptr ) { insert_node( this->root, item ); }

   else{ this->root = new_node( item ); }
   // si el árbol está vacío, creamos el nodo raíz
   // ¡este paso es obligatorio, no nos lo podemos saltar!

   ++this->len;

   return true;
}

template<typename T>
bool BST<T>::Seek( T& item )
{
   return seek_node( this->root, item ) != nullptr;
}

template<typename T>
void BST<T>::Traverse( BST::Dir dir, void (*visit)( T& item ) )
{
   if( this->root != nullptr ){
      switch( dir ){
         case Dir::INORDER:   inorder(   this->root, visit ); break;
         case Dir::PREORDER:  preorder(  this->root, visit ); break;
         case Dir::POSTORDER: postorder( this->root, visit ); break;
      }
   }
}

template<typename T>
Node<T>* BST<T>::Get_root()
{
   return this->root;
}

template<typename T>
bool BST<T>::Remove( T& item )
{
   Node<T>* ret_val = remove( this->root, item );

   if( ret_val != nullptr )
   {
      --this->len;
   }

   return ret_val != nullptr;
}

template<typename T>
T& BST<T>::Predecessor( T& key )
{
   return predecessor( this->root, key )->item;
}

template<typename T>
T& BST<T>::Min()
{
   Node<T>* node = this->root;

   while( node->left != nullptr ) node = node->left;

   return node->item;
}

template<typename T>
T& BST<T>::Max()
{
   Node<T>* node = this->root;

   while( node->right != nullptr ) node = node->right;

   return node->item;
}

template<typename T>
size_t BST<T>::Height()
{
   return calc_height( this->root, 0 );
}
